from tkinter import *
from tkinter import messagebox
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException
chrome_options = Options()
chrome_options.add_argument("--disable-javascript")
chrome_options.add_argument("--headless")
import time
import requests
from bs4 import BeautifulSoup

default_font = ('Arial', 10)

def get_team_page(team):
    try:
        driver = webdriver.Chrome(chrome_options=chrome_options)
        driver.get("http://www.eredmenyek.com")
        driver.find_element_by_class_name('header__buttonIcon--search').click()
        time.sleep(0.5)
        driver.find_element_by_id('search-form-query').send_keys(team, Keys.RETURN)
        time.sleep(0.5)
        try:
            driver.find_element_by_xpath('//div[@id="search-results"]//a[1]').click()
        except NoSuchElementException:
            messagebox.showerror("Error", f'Couldn\'t find a team with the name: {team}')
            raise
        return driver.page_source
    finally:
        print('quit driver')
        driver.quit()

def parse_page_text(page_source):
    soup = BeautifulSoup(page_source, 'html.parser')
    f = soup.find('div', text='Hátralévő')
    f = f.find_next('span', class_='event__title--name')
    event = f.string
    f = f.find_next('div', class_='event__time')
    time = f.string
    f = f.find_next('div', class_='event__participant--home')
    home = f.string
    f = f.find_next('div', class_='event__participant--away')
    away = f.string
    print(f'time={time} home={home} away={away}')
    return f'{event}\n{home} - {away}\n{time}'

def get_data(team):
    global data
    page_source = get_team_page(team)
    data = parse_page_text(page_source)

def start_gui():
    def change_text(widget, text):
        widget['text'] = text
    window = Tk()
    window.geometry('350x200')
    window.title('Next match')
    lbl = Label(window, text='Gimme the next match of', font = default_font)
    lbl.grid(column = 0, row = 0)
    txt = Entry(window, width = 10, font = default_font)
    txt.grid(column = 1, row = 0)
    txt.insert(END, 'real madrid')
    message = Message(window, font = default_font, width = 200)
    message.grid(column = 0, row = 1, columnspan = 3)
    btn = Button(window, text='Now!', font = default_font, command = lambda: (get_data(txt.get()), change_text(message, data)))
    btn.grid(column = 2, row = 0)
    window.mainloop()


if __name__ == '__main__':
    data = 'init'
    start_gui()
